file_conc_cf <- file.path(test_path("../../devtemp/extdata"), "meteo_wrf.nc")
file_meteo_cf <- file.path(test_path("../../devtemp/extdata"), "meteo_wrf.nc")
file_c_h_ioapi <- file.path(test_path("../../devtemp/extdata"), "MAD_c_2015_SVR_Hourly_month01.nc")
file_c_y_ioapi <- file.path(test_path("../../devtemp/extdata"), "MAD_c_2015_SVR_Yearly.nc")
file_meteo_ioapi <- file.path(test_path("../../devtemp/extdata"), "MAD_meteo_2015_SVR_Hourly_month01.nc")

check_netcdf <- function() {
    # Check if netcdf files exist
    if (!file.exists(file_conc_cf))
        skip("NetCDF file 1 not available")
    if (!file.exists(file_meteo_cf))
        skip("NetCDF file 2 not available")
    if (!file.exists(file_c_h_ioapi))
        skip("NetCDF file 3 not available")
    if (!file.exists(file_meteo_ioapi))
        skip("NetCDF file 4 not available")
    if (!file.exists(file_c_y_ioapi))
        skip("NetCDF file 5 not available")

}

test_that("Name of variables", {
    skip_on_cran()
    check_netcdf()
    # meteo wrf
    expect_equal(nc_variables(file_meteo_cf)[["name"]],
                 c("time", "x", "y", "z", "REL", "SST", "TCC", "PREC", "PHI",
                   "U", "V", "W", "T", "P", "RH", "DIV"))
    # conc cmaq hourly
    expect_equal(nc_variables(file_c_h_ioapi)[["name"]],
                 c("TFLAG", "NO2", "O3", "PM25", "PM10"))
    # conc cmaq yearly
    expect_equal(nc_variables(file_c_y_ioapi)[["name"]],
                 c("TFLAG", "NO2", "O3", "PM10", "PM25", "BSOA", "ISOP", "TERP"))

    # meteo cmaq
    expect_equal(nc_variables(file_meteo_ioapi)[["name"]],
                 c("TFLAG", "RH", "T2", "PBLH", "WSPD10"))
})

test_that("Number of deadlines", {
    skip_on_cran()
    check_netcdf()

    expect_length(nc_deadlines(file_conc_cf), 24)
    expect_length(nc_deadlines(file_meteo_cf), 24)
    expect_length(nc_deadlines(file_c_y_ioapi), 1)
    expect_length(nc_deadlines(file_c_h_ioapi), 745)
    expect_length(nc_deadlines(file_meteo_ioapi), 745)
})

test_that("First deadline is POSIXt", {
    skip_on_cran()
    check_netcdf()

    expect_s3_class(nc_deadlines(file_conc_cf)[[1]], "POSIXt")
    expect_s3_class(nc_deadlines(file_meteo_cf)[[1]], "POSIXt")
    expect_s3_class(nc_deadlines(file_c_y_ioapi)[[1]], "POSIXt")
    expect_s3_class(nc_deadlines(file_c_h_ioapi)[[1]], "POSIXt")
    expect_s3_class(nc_deadlines(file_meteo_ioapi)[[1]], "POSIXt")
})

test_that("Dimensions names", {
    skip_on_cran()
    check_netcdf()

    expect_equal(nc_dimensions(file_meteo_cf)[["name"]],
                 c("time", "x", "y", "z"))
    expect_equal(nc_dimensions(file_meteo_ioapi)[["name"]],
                 c("TSTEP", "DATE-TIME", "LAY", "VAR", "ROW", "COL"))
    expect_equal(nc_dimensions(file_c_h_ioapi)[["name"]],
                 c("TSTEP", "DATE-TIME", "LAY", "VAR", "ROW", "COL"))
    expect_equal(nc_dimensions(file_c_y_ioapi)[["name"]],
                 c("TSTEP", "DATE-TIME", "LAY", "VAR", "ROW", "COL"))
})

test_that("get variable length", {
    skip_on_cran()
    check_netcdf()

    expect_length(nc_get_variable(file_meteo_cf, "T"), 5)
    expect_length(nc_get_variable(file_meteo_cf, "T")[[1]], 960000)

    expect_length(nc_get_variable(file_c_h_ioapi, "NO2"), 5)
    expect_length(nc_get_variable(file_c_h_ioapi, "NO2")[[1]], 14590080)

    expect_length(nc_get_variable(file_c_y_ioapi, "NO2"), 5)
    expect_length(nc_get_variable(file_c_y_ioapi, "NO2")[[1]], 19584)

    expect_length(nc_get_variable(file_meteo_ioapi, "T2"), 5)
    expect_length(nc_get_variable(file_meteo_ioapi, "T2")[[1]], 14590080)
})

test_that("Variable does not exist", {
    skip_on_cran()
    check_netcdf()

    expect_error(nc_get_variable(file_meteo_cf, "NO3"))
    expect_error(nc_get_variable(file_c_y_ioapi, "NO3"))
})

test_that("nc_extract is working", {
    skip_on_cran()
    check_netcdf()

    ddd <- nc_get_variable(file_meteo_cf, "T")
    ddd[, `:=`(x = x * 1000, y  = y * 1000)]
    obs <- data.frame(stn = c("alpha", "beta", "gamma"),
                      x = c(685000,  6970000,  681010),
                      y = c(4932111, 4942501, 4922050))
    estr <- nc_extract(mydata = ddd, name.value = "T",
                       stations = obs, st.code = "stn")
    setorder(estr, time, stn)
    expect_length(estr, 5)
    expect_length(estr[[1]], 48)
    expect_equal(estr[1, "T"][["T"]], 258.06354)
    expect_equal(estr[nrow(estr), "T"][["T"]], 255.9415)
})
