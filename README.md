
<!-- README.md is generated from README.Rmd. Please edit that file -->

# AirVeg

<!-- badges: start -->
<!-- badges: end -->

*AirVeg* is a set of tools to import, analyse and validate air quality
and meteorological data, both from numerical simulations and
observations. The package has been developed in the framework of the
[VEG-GAP project](https://www.lifeveggap.eu) and all the initial
development choices have been taken by taking into account the needs of
the participant to the project.

*AirVeg* is currently maintained by
[Simularia](https://www.simularia.it) and it is free open source
software.

## Credits

This packages has been partially developed with the financial
contribution of **ENEA** within the framework of VEG-GAP Project -
LIFE18 PRE IT 003. For further information visit the [VEG-GAP project
website](https://www.lifeveggap.eu).

## Installation

`AirVeg` is currently not on CRAN, therefore you have to install it from
the repository on [GitLab](https://gitlab.com/simularia/veg-gap/airveg):

``` r
install.packages("remotes")
remotes::install_gitlab(repo="simularia/veg-gap/airveg", dependencies = TRUE)
```

Alternatively, you can download the *zip* archive of the sources and
install them like that:

``` r
remotes::install_local(path="airveg-master.zip", dependencies = TRUE)
```

To install the *development* version:

``` r
remotes::install_gitlab(repo="simularia/veg-gap/airveg@develop", dependencies = TRUE)
```

All dependencies are automatically installed from CRAN.

## Brief description

`AirVeg` is a collection of tools to pre and post process data from air
quality models, meteorological models and measurements.

A full description of each function included in the package is provided
in the integrated help. For example, type:

``` r
? nc_get_variable
```

to see the corresponding documentation, with full details, arguments
description and examples.

The whole set of included functions is logically organized in five main
groups.

1.  Read and import data from **NetCDF** binary archives

This group of functions are included in the *netcdfutils.R* file:

    nc_deadlines()
    nc_dimensions()
    nc_extract()
    nc_get_CRS()
    nc_get_variable()
    nc_summary()
    nc_variables()

All the functions are compatible with input files in Climate and
Forecast (CF) convention and with `I/O API` used by *CMAQ*.

2.  Read and import data and metadata from **measurements** and
    point-like dataset.

Two subsets of functions are provided. In the *observations.R* file,
functions are provided to deal with measurements:

    import_EEA()
    import_data_EEA()
    import_data_arpameteo()
    import_data_brace()
    import_metadata_EEA()
    import_metadata_arpameteo()
    import_metadata_brace()
    download_data_EEA()
    download_metadata_EEA()

In the *models.R* source file, functions are provided to read data from
the *postbin* tool used in the VEG-GAP project:

    import_data_postbin()
    import_metadata_postbin()
    import_postbin()

3.  Data processing and other utilities

The following functions are included in the *time_series.R* file:

    timeAggregation()
    check_validity()
    check_aggregation_validity()

The following utilities are included in the *utils.R* file:

    ppb2ugm3()
    projectCoordinates()
    season()
    windDir()

4.  Statistical scores and other statistical functions

The following functions are included in the *stats.R* file:

    aot40f()
    aot40v()
    bias()
    computeMode()
    corr()
    countComplete()
    fac2()
    fb()
    ioa()
    mage()
    mange()
    maxRunningValue()
    mfb()
    mfe()
    mqi()
    mqi_year()
    nmse()
    rmse()
    rmsu()
    runningEightHoursMean 
    unc_year()

5.  Graphical output

The following functions are included in the *graphics.R* file:

    boxPlot()
    buglePlot()
    contourPlot()
    scatterPlot()

## Contributing

All contributions, in any form, are welcome!

To submit new features just open a pull request in the main repo on
[gitlab](https://gitlab.com/simularia/veg-gap/airveg). For any other
reason, bug report, feature request, etc. file an issue.
