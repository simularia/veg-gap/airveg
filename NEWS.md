# AirVeg 0.10.1

- `nc_` functions support files from the [MERIDA](https://merida.rse-web.it) dataset.
- Add documentation site.
- Update authors. Add ENEA and Simularia as project funders.
- Fix examples in `aot40f()` and `aot40v()` and other functions. Also formulas are now correctly rendered.
- Add introductory vignette by splitting the readme file.
- Rename CHANGELOG to NEWS.

# AirVeg 0.9.0

- New function `windDir()` to compute wind direction from its components.
- It is now possible to read data from CF NetCDF files with only two dimensions.
- Bugfix in `check_aggregation_validity`.
- Bugfix in `nc_summary()` for CF files, where coordinates where not properly
sorted.
- `nc_summary()` understand `latitude`, `longitude`, `easting` and `northing`
as dimension names.
- `nc_get_variable()` (CF convention) now correctly manages the `scale_factor` 
and `add_offset` attributes for variables.
- 2D Latitude, Longitude, Coordinate Variables are not correctly read by
`nc_` functions. `nc_get_variables()` now returns a simple sequence of indeces.
The 2D coordinate variables can be read as fields themselves.

# AirVeg 0.8.1

- Fixed compatibility with latest `terra` package versions. `terra` minimum 
version `1.6-7` is required.
- Fixed syntax errors in the documentation.

# AirVeg 0.8.0
- Added two specialized types in `check_aggregation_type`: *monthly* and *semester*.
- Added automatic testing for `check_validity()` and `check_aggregation_validity()`.
- Updated documentation
- Minor bugfixes

# AirVeg 0.7.2
- Bugfix in `check_validity()` when data are not time ordered.
- Documented argument `na.rm` in `computeMode()`.

# AirVeg 0.7.1
- `na.rm` optional argument in `computeMode()`

# AirVeg 0.7.0
- BREAKING CHANGE: updated *check_validity()*: removed verbosity argument; not
    valid values are substituted with NA's.
- BREAKING CHANGE: updated *check_aggregration_validity()*.
- BREAKING CHANGE: updated *season()* utility function.
- New function `runningEightHoursMean()`.
- New function `computeMode()` to compute the mode of a data set.
- `import_data_brace()` now supports a `timebegin` boolean flag to time shift
concentrations from *time-begin* to *time-end* as it is the case for dispersion
model data.
- DEPRECATED: Given the evolution and the typical usage of *AirVeg* the functions
*timeAggregation()* and *timeAggregation_single()* are now deprecated and not 
maintained anymore, in favour of base functions to be applied on data.frame columns.

# AirVeg 0.6.0
- Added `aot40v` and `aot40f`.
- Updated README file.

# AirVeg 0.5.2
- Updated this changelog.

# AirVeg 0.5.1
- New function `nc_get_CRS()` to get crs of a netcdf file (it currently does 
not work with CF).
- Fixed some bugs in `nc_extract()`.
- Check for duplicates in `arpa_meteo_stations()`.

# AirVeg 0.5.0
- BREAKING: Updated `projectCoordinates` argument names.
- Greatly improved compatibility with IO/API files (CMAQ).
- `nc_summary()` for CMAQ files prints more information.
- Removed dependency from `rgdal` and `raster` in favour of `terra`.
- Updated readme file.
- Fixed many syntax errors in documentation.
- Introduced some automatic testing.

# AirVeg 0.4.2
- Updated documentation and readme file, citing Veg-Gap project.
- Updated license to GPL-v3 to facilitate sharing improvements.

# AirVeg 0.4.1
- Fixed bug in `ppb2ugm3()`.

# AirVeg 0.4.0
- BREAKING: `countCompleteCouples` renamed to `countComplete`.
- BREAKING: `unc` renamed to `unc_year`.
- BREAKING: Changed some argument names in stat functions to improve overall consistency.
- BREAKING: Modified the arguments of `boxPlot` for improved clarity and consistency. 
- BREAKING: Bugfixes and arguments update of `buglePlot`.
- BREAKING: Removed `Boylan` formulation from `mfe` and `mfb`.
- BREAKING: Changed some arguments in `timeAggregation` for improved clarity and overall consistency.
- Bugfixes in `timeAggregation`.
- Fixed bug in `mqi`.
- Improved README documentation. Also added a validation example.
- Various minor bugfixes and code cleanup.
- Many updates to documentation and some spell checking.

# AirVeg 0.3.2
- Fixed bug in deadline decoding for non-CF NetCDF archives.
- Cleaned up code in nc_ functions. Getting NetCDF dimensions and variables is now more reliable 
with many different formats.
- Updated README file.

# AirVeg 0.3.1
- Fixed bug in nc_summary for some types of NetCDF files.
- Fixed bug in nc_get_variable with some older NetCDF-3 COARDS files. Closes #16.

# AirVeg 0.3.0
- Parallelization of `nc_get_variable`.
- Added `maxRunningValue` to compute the maximum number consecutive entries above, equal or lower 
than a given threshold.
- `rmse` and `bias` accept `use` optional argument (not `na.rm` anymore). Closes #29.
- `nc_extract` is now partly parallelized.
- Fixed bug in `nc_extract` not merging model data with observations.
- Fixed various minor bugs.

# AirVeg 0.2.6
- Added a brief description of exported functions.
- Fixed bug #37 in `import_EEA`: *pollutant* argument is mandatory in order to merge stations with observations.
- Fixed bug #33 in `projectCoordinates`
- Removed *verbose* argument in `import_data_EEA`.
- Fixed bugs in filtering stations in `import_metadata_EEA`.
- Added wrapper to `contourPlot2()` from `simulariatools`.
- Updated readme file and documentation.

# AirVeg 0.2.5
- `transfCoords()` renamed to `projectCoordinates()` and removed dependency to `proj4` library.
- Bugfixes. 

# AirVeg 0.2.4
- Starting this *CHANGELOG*.
- `nc_get_variable()` accepts files, folders and list of files as input. It also accepts a single or
a list of `NetCDF` objects as read by `RNetCDF::open.nc`.
- The value column in the data.table returned by `nc_get_variable` is now named after the `variable`
input argument.
- `nc_extract()` autmoatically merges model data and observations if the name of the column with 
observations is provided through the `st.value` argument.
- Various bugfixes.
